import {Borehead} from "./Borhead";
import {Stabilization} from "./Stabilization";

export class Manager {
    private stepDuration: number;
    private budget: number;
    private cellBudgetFaktor;

    constructor() {
        this.budget = 25000;
        this.stepDuration = 5000;
        this.cellBudgetFaktor = 1;
    }

    buyBorehead(borehead: Borehead) {
        if ( this.budget >= borehead.getPrice() ) {
            this.budget -= borehead.getPrice();
            return true;
        } else {
            return false;
        }
    }

    buyExploration() {
        let price = 4000;
        if ( this.budget >= price ) {
            this.budget -= price;
            return true;
        } else {
            return false;
        }
    }

    buySeismics() {
        let price = 2000;
        if ( this.budget >= price ) {
            this.budget -= price;
            return true;
        } else {
            return false;
        }
    }

    buyStabilization(stabilization: Stabilization) {
        if ( this.budget >= stabilization.getPrice() ) {
            this.budget -= stabilization.getPrice();
            return true;
        } else {
            return false;
        }
    }

    getBudget() {
        return this.budget;
    }
    
    getCellBudget( strength : number ) {
        return this.cellBudgetFaktor*strength;
    }
    
    payBoremeter( strength : number ) {
        let neededBudget = this.getCellBudget(strength);
        if ( this.budget < neededBudget) {
            return false;
        }
        this.budget -= neededBudget;
        return true;
    }
    
    increaseBudget(value : number) {
        this.budget += value;
    }
}
