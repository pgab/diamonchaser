cmake_minimum_required(VERSION 2.8)
project (levelgen)

set(
LODEPNG_DIR
	lodepng
	CACHE PATH "LODEPNG root path"
)
include_directories(${LODEPNG_DIR})

set(
MAIN_SRCS 
	main.cpp
)

add_executable(levelgen 
	${MAIN_SRCS} ${LODEPNG_DIR}/lodepng.cpp
)