#include "lodepng.h"
#include <iostream>
#include <string>
#include <fstream>

void png2json(const std::string &pngFile, const std::string &jsonOut,
              const std::string &greyOut, unsigned int locationU,
              unsigned int locationV) {
  std::vector<unsigned char> image; //the raw pixels
  std::vector<unsigned char> imageStrength;
  unsigned width, height;

  //png decode
  unsigned error = lodepng::decode(image, width, height, pngFile.c_str());
  imageStrength.resize(width*height*4);

  //if there's an error, display it
  if(error) 
  {
	  std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
	  return;
  }

  std::ofstream jsonFile(jsonOut.c_str());
  jsonFile << "var loadedLevel = {" << std::endl;

  jsonFile << "  \"boreHead\": {" << std::endl;
  jsonFile << "    \"u\": "<<locationU<<"," << std::endl;
  jsonFile << "    \"v\": "<<locationV<< std::endl;
  jsonFile << "  }," << std::endl;

  jsonFile << "  \"width\": "<<width<<"," << std::endl;
  jsonFile << "  \"height\": "<<height<<"," << std::endl;

  jsonFile << "  \"grid\": [" << std::endl;

  //the pixels are now in the vector "image", 4 bytes per pixel, ordered RGBARGBA..., use it as texture, draw it, ...
  unsigned idx;
  bool first = true;
  unsigned int red, green, blue, alpha, strength;
  bool visible;
  bool drilled=false;
  bool target=false;
  unsigned int targetCount = 0;
  unsigned int visibleCount = 0;
  const unsigned int pixelsTotal = width*height;
  for (unsigned col = 0; col < height; ++col) 
  {
    for (unsigned row = 0; row < width; ++row) 
	{
      idx = col * width * 4 + row*4;
	  red = (unsigned int)image[idx];
	  green = (unsigned int)image[idx+1];
	  blue = (unsigned int)image[idx+2];
	  alpha = (unsigned int)image[idx+3];

	  strength = 1000.0f - (float)(red+green+blue)/(float)(255*3)*1000.0f;
	  visible = alpha < 128;
	  target = (red<5 && green < 5 && blue < 5);

	  if(visible) {
		  visibleCount++;
	  }
	  if(target) {
		  targetCount++;
	  }

	  imageStrength[idx] = 255-(float)strength/1000.0f*255.0f;
	  imageStrength[idx+1] = 255-(float)strength/1000.0f*255.0f;
	  imageStrength[idx+2] = 255-(float)strength/1000.0f*255.0f;
	  imageStrength[idx+3] = 255;

      if (first) {
        first = false;
      } else {
        jsonFile << "," << std::endl;
      }

      jsonFile << "     ["
		  << (visible ? "true": "false") << ", " 
		  << strength << ", "
		  << (drilled ? "true": "false") << ", " 
		  << (target  ? "true": "false") << "]";
    }
  }

  jsonFile << std::endl;
  jsonFile << "  ]" << std::endl;
  jsonFile << "};" << std::endl;

  jsonFile.close();

  std::cout << pixelsTotal << " (" << width << " x " << height << ") " <<  " pixels written." <<std::endl;
  std::cout << visibleCount << " (" << (float)visibleCount/pixelsTotal*100.0f << " %) are visible." <<std::endl;
  std::cout << targetCount << " (" << (float)targetCount/pixelsTotal*100.0f << " %) are target pixel." <<std::endl;

  if(targetCount == 0)
  {
	  std::cout << "WARINING: LEVEL HAS NO TARGET AREA !!!!" << std::endl;
  }

  //write strength file
  unsigned error2 = lodepng::encode(greyOut, imageStrength, width, height);
  if(error2) 
  {
	  std::cout << "encode error " << error << ": " << lodepng_error_text(error) << std::endl;
	  return;
  }
}

int main(int argc, char *argv[])
{
  std::string filenameStump = argc > 1 ? argv[1] : "sample";
  unsigned int start_U = atoi(argc > 2 ? argv[2] : 0);
  unsigned int start_V = atoi(argc > 3 ? argv[3] : 0);


  png2json(filenameStump+".png", filenameStump+".js", filenameStump+"_strength.png", start_U, start_V);
}

