import { UsableItems } from './UsableItems'

export class Borehead extends UsableItems {
    private speed: number;
    private price: number;

    constructor(durabilityDecrement:number, speed: number, price: number) {
        super(durabilityDecrement);
        this.speed = speed;
        this.price = price;
    }

    getSpeed(speed) {
        this.speed = speed;
    }
    getPrice() {
        return this.price;
    }
    
    use(strength : number) {
        return super.use(strength) &&
        super.getManager().payBoremeter(strength);
    }
}

export function getBorehead(boreheadS : string) : Borehead{
    var borehead;
    if(boreheadS == 'iron') {
        borehead = new Borehead(1, 1, 1000);
    } else if(boreheadS == 'steel') {
        borehead = new Borehead(0.5, 1.2, 2000);
    } else if(boreheadS == 'diamond') {
        borehead = new Borehead(0.25,1.4, 4000);  
    } else {
        // throw?
    }
    return borehead;
}

// values taken from http://tekkitclassic.wikia.com/wiki/Bore_Head
export let IronBorehead = new Borehead(1, 1, 1000);
export let SteelBorehead = new Borehead(0.5, 1.2, 2000);
export let DiamondBorehead = new Borehead(0.25,1.4, 4000);
export let TitaniumBorehead = new Borehead(0.25, 1.4, 4000);