import { UsableItems } from './UsableItems'

export class Stabilization extends UsableItems {
    private price: number;
    private maxPressure: number;

    constructor(durabilityDecrement: number, price: number) {
        super(durabilityDecrement);
        this.price = price;
    }

    getPrice() {
        return this.price;
    }
    getMaxPressure() {
        return this.maxPressure;
    }
    
    
}

export function getStabilization(stabilizationS : string) : Stabilization {
    var stabilization;
    if(stabilizationS == 'wood') {
        stabilization = new Stabilization(1, 100);
    } else if(stabilization == 'concrete') {
        stabilization = new Stabilization(4, 400);
    } else if(stabilization == 'steel') {
        stabilization = new Stabilization(6, 600);
    }
    return stabilization;
}

// @todo: these values could use some improvement
// i dont know if these are good numbers
export let Wood = new Stabilization(1, 100);
export let Concrete = new Stabilization(4, 400);
export let Steel = new Stabilization(6, 600);
export let SteelConcrete = new Stabilization(8, 1000);