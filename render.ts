import {GridPoint, Map, Engine} from "./engine"
declare var renderer : Renderer;
declare var icons;

export class Renderer{
    private width;
    private height;
    
    ctx: CanvasRenderingContext2D;
    canvas;

    private onClickCallback;
    private cellcountX;
    private cellcountY;

    constructor(canv, img_url: string){
        canv.style.backgroundImage = "url("+img_url+")";
        var that = this;
        canv.onclick = function(e){
            that.internal_onclick(e, canv);
        }
        this.canvas = canv;
        this.width = canv.width;
        this.height = canv.height;
        this.ctx = canv.getContext("2d");
        this.ctx.save();
    }

    setOnClick(onclick){
        this.onClickCallback = onclick;
    }

    private internal_onclick(e, canv) {
        let width = this.canvas.getBoundingClientRect().width;
        let height = this.canvas.getBoundingClientRect().height;
        if (this.onClickCallback){
            var x;
            var y;
            if (e.pageX || e.pageY) { 
                x = e.pageX;
                y = e.pageY;
            } else { 
                x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
                y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
            } 
            x -= canv.offsetLeft;
            y -= canv.offsetTop;
            let cellWidthX = width / this.cellcountX;
            let cellWidthY = height / this.cellcountY;
            x = Math.floor(x / cellWidthX);
            y = Math.floor(y / cellWidthY);
            this.onClickCallback(new GridPoint(x,y))
        }
    }
    
    render(what: Map) {
        this.cellcountX = what.width;
        this.cellcountY = what.height;
        let cellWidthX = this.width / what.width;
        for (var x = 0; x< what.width; x++){
            let cellWidthY = this.height /what.height;
            for(var y=0; y< what.height; y++){
                if (what.visibility(x,y)){
                    this.ctx.clearRect(x * cellWidthX, y * cellWidthY, cellWidthX, cellWidthY);
                    if (what.resource(x,y)){
                        let r = what.resource(x,y);
                        if (r.name == 'gold'){
                            this.ctx.drawImage(icons[0], x * cellWidthX, y * cellWidthY, cellWidthX, cellWidthY);
                        } else if (r.name == 'diamond'){
                            this.ctx.drawImage(icons[1], x * cellWidthX, y * cellWidthY, cellWidthX, cellWidthY);
                        } else {
                            this.ctx.drawImage(icons[2], x * cellWidthX, y * cellWidthY, cellWidthX, cellWidthY);
                        }
                    }
                    if (what.drilled(x,y)){
                        this.ctx.beginPath();
                        this.ctx.fillStyle="rgba(77,77,77, 0.5)";
                        this.ctx.rect(x * cellWidthX, y * cellWidthY, cellWidthX, cellWidthY);
                        this.ctx.fill();
                    }
                } else {
                    this.ctx.beginPath();
                    this.ctx.fillStyle="#d3d3d3";
                    this.ctx.strokeStyle="#d3d3d3";
                    this.ctx.rect(x * cellWidthX, y * cellWidthY, cellWidthX+1, cellWidthY+1);
                    this.ctx.fill();
                }
                if (what.target(x,y)){
                }

            }
        }
    } 

};

declare var engine: Engine;
declare var gold;

export function init_render(level: number){

    var levelStumpStr = "./levels/testlevel0" + level.toString();
    
    var canv =  <HTMLCanvasElement> document.getElementById("canvas");
    renderer = new Renderer(canv, levelStumpStr+"_texture.png");

    engine = new Engine(level);

    renderer.setOnClick(function (p: GridPoint){
        engine.updateValidMoves();
        engine.nextMove(p);
    });
    icons = [new Image(), new Image(), new Image()];
    icons[0].src = "icons/gold.png";
    icons[1].src = "icons/diamond.png";
    icons[2].src = "icons/fossil.png";
}
