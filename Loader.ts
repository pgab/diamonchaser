import {Engine, Map} from "./engine";
import {Renderer} from "./render";
import {Resource} from "./Resource"

declare var renderer : Renderer;
declare var engine: Engine;
declare var loadedLevel;

export class Loader{
    private map: Map;


    loadFile(url: string) {
        let script = document.createElement('script');
        script.src=url;
        document.getElementsByTagName('head')[0].appendChild(script);

        /*
        var req = new XMLHttpRequest();
        req.addEventListener("load", this.onLoad.bind(this, req));
        req.addEventListener("progress", this.onProgress);
        req.addEventListener("error", this.onError);
        req.addEventListener("abort", this.onError);
        // we use a sync loading since we dont want to work with
        // promises here
        req.open('GET', url, false);

        req.send();*/
        script.onload = function() {
            engine.map = new Map();
            engine.map.width = loadedLevel.width;
            engine.map.height = loadedLevel.height;
            engine.map.grid = loadedLevel.grid.map((v) => {
                return [v[0], v[1], v[2], v[3], null];
            });
            engine.map.boreHead = loadedLevel.boreHead;
            engine.map.boreDirection = loadedLevel.boreDirection;
            engine.map.set_visibility(loadedLevel.boreHead.u, loadedLevel.boreHead.v, true);
            engine.map.set_drilled(loadedLevel.boreHead.u, loadedLevel.boreHead.v, true);
            engine.rick = loadedLevel.boreHead;
            fix_rick();
            renderer.render(engine.map);
            engine.placeResources();
            engine.updateVisibility.bind(engine, engine.map.boreHead);
            engine.updateValidMoves.bind(engine);            
        };
        return this.map;

    }

    // the keyword this refers here to the request, not the class :(
    onLoad(req: XMLHttpRequest) {
        if ( req.responseText !== undefined ) {
            try {
                // preserve newlines, etc - use valid JSON
                let s = req.responseText.replace(/\\n/g, "\\n")
                    .replace(/\\'/g, "\\'")
                    .replace(/\\"/g, '\\"')
                    .replace(/\\&/g, "\\&")
                    .replace(/\\r/g, "\\r")
                    .replace(/\\t/g, "\\t")
                    .replace(/\\b/g, "\\b")
                    .replace(/\\f/g, "\\f");
                // remove non-printable and other non-valid JSON chars
                s = s.replace(/[\u0000-\u0019]+/g,"");
                var obj = JSON.parse(s);
                this.map = new Map();
                this.map.grid = obj.grid;
                this.map.width = obj.width;
                this.map.height = obj.height;
                this.map.boreHead.u = obj.boreHead.u;
                this.map.boreHead.v = obj.boreHead.v;
            } catch (ex) {
                console.log('Conversion to map failed: ');
                console.log(ex);
            }
        } else {
            console.log('Sth is rotten in the state of Denmark');
        }
    }

    onProgress(evt) {
        if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            // ...
        } else {
            // Unable to compute progress information since the total size is unknown
        }
    }

    onError(e) {
        console.log("An error occurred:");
        console.log(e);
    }
}

export function fix_rick(){
    var canv = <HTMLCanvasElement> document.getElementById("canvas");
    var rick = <HTMLImageElement> document.getElementById("rick");

    let cellsize = (canv.getBoundingClientRect().width / engine.map.width);

    let r = (cellsize / 2) - (rick.width/2);
    let left_border =  cellsize * engine.rick.u + r;
    
    rick.style.paddingLeft = String(left_border)+"px";

}
