import {Borehead, getBorehead} from "./Borhead";
import {Stabilization, getStabilization} from "./Stabilization";
import {Manager} from './Manager'
import {Loader} from './Loader'
import {Resource, makeResource} from './Resource'
import {MAX_DURA} from './UsableItems'

declare var renderer;

export class GridPoint {
    u : number;
    v : number;
    constructor(u : number, v : number) {
        this.u = u;
        this.v = v;
    }
}


export class Map {
    width : number;
    height : number;
    grid: [[boolean, number, boolean, boolean, Resource]];
    boreHead : GridPoint;
    boreDirection : GridPoint;
    
    private cell(u : number, v : number) {        
        return this.grid[v * this.width + u];
    }

    set_visibility(u: number, v: number, val: boolean){
        this.cell(u,v)[0] = val;
    }
    
    set_visibilityToAll(val: boolean) {
        for(var u=0; u<this.width; ++u)
        {
            for(var v=0; v<this.height; ++v)
            {
                this.set_visibility(u,v,val);
            }
        }
    }
    
    visibility(u : number, v : number) {
        return this.cell(u,v)[0];
    }

    strength(u : number, v : number){
        return this.cell(u,v)[1];
    }

    set_drilled(u : number, v : number, val: boolean){
        this.cell(u,v)[2]= val;
    }
    
    drilled(u : number, v : number){
        return this.cell(u,v)[2];
    }

    target(u : number, v : number){
        return this.cell(u,v)[3];
    }
    
    set_resource(u: number, v : number, resource : Resource) {
        this.cell(u,v)[4] = resource
    }
    
    resource(u : number, v : number) {
        return this.cell(u,v)[4];
    }
    
    constructor() {
        this.width = 0;
        this.height = 0;
        this.grid = [[true, 1, true, true, null]];
        this.boreHead = new GridPoint(0, 0);
        this.boreDirection = new GridPoint(0, 1);
    }
}

export class PathPoint {
    point : GridPoint;
    stabilization : Stabilization;
    constructor(point : GridPoint) {
        this.point = point;
        this.stabilization = null;
    }
}

export class Engine {
    map : Map;
    path : PathPoint[];
    currentVisible : GridPoint[];
    validMoves : GridPoint[];
    visibiltyRange : number;
    currentBorehead : Borehead;
    manager : Manager;
    rick: GridPoint;

    levels : string[];
    currentLevel : number;
    loader : Loader;
    timeout: number;
    gui;
    timer: number;
    timeLeft: number;
    stepsTaken: number;
    constructor(level: number) {
        this.path = [];
        this.gui = {};
        this.currentVisible = [];
        this.visibiltyRange = 1;
        this.currentBorehead = getBorehead('iron');
        this.manager = new Manager();
        this.currentBorehead.setManager(this.manager);
        this.levels = ['./levels/testlevel01.js', './levels/testlevel02.js', './levels/testlevel03.js'];
        this.loader = new Loader();
        this.map = this.loader.loadFile(this.levels[level-1]);
        this.currentLevel = level-1;
        this.timer = 0;
        this.timeLeft = 0;
        this.stepsTaken = 0;
        this.setupGUI();
        this.setTimeout();
        window.addEventListener('keydown', this.onKeyDown.bind(this));
    }
    onKeyDown(e) {
        switch(e.keyCode) {
            case 37: // left
                this.nextMove(new GridPoint(this.map.boreHead.u-1, this.map.boreHead.v));
                break;
            case 39: // right
                this.nextMove(new GridPoint(this.map.boreHead.u+1, this.map.boreHead.v));
                break;
            case 40: // down
                this.nextMove(new GridPoint(this.map.boreHead.u, this.map.boreHead.v+1));
                break;
        }
    }
    buyNewBorehead(boreheadS : string) {
        var borehead = getBorehead(boreheadS);
        if(this.manager.buyBorehead(borehead)) {
            this.currentBorehead = borehead;
            this.currentBorehead.setManager(this.manager);
            this.gui.durability.MaterialProgress.setProgress(100);
            this.gui.boreheadType.innerHTML = boreheadS;
        } else {
            this.displayMessage('Not enough Money');
        }
    }
    buyStabilization(stabilizationS : string) {
        var stabilization = getStabilization(stabilizationS);
        if(this.manager.buyStabilization(stabilization)) {
            this.path[this.path.length - 1].stabilization = stabilization;
        } else {
            this.displayMessage('Not enough Money');
        }
    }
    updateValidMoves() {
        this.validMoves = [];
        var bu = this.map.boreHead.u;
        var bv = this.map.boreHead.v;
        if(bu > 0){
            this.validMoves.push(new GridPoint(bu - 1, bv))
            if(bv < this.map.height - 1) {
                this.validMoves.push(new GridPoint(bu - 1, bv + 1))
            }
        }
        if(bu < this.map.width - 1) {
            this.validMoves.push(new GridPoint(bu + 1, bv))
            if(bv < this.map.height - 1) {
                this.validMoves.push(new GridPoint(bu + 1, bv + 1))
            }
        }
        if(bv < this.map.height - 1) {
            this.validMoves.push(new GridPoint(bu, bv + 1))
        }
        this.validMoves = this.validMoves.filter((p) => {
            return this.map.drilled(p.u, p.v) == false;
        });
    }
    autoMove() {
        var r = Math.floor(Math.random()*this.validMoves.length);
        this.nextMove(this.validMoves[r]);
        
    }
    nextMove(point : GridPoint) {
        this.clearTimeout();
        if(!this.validMoves.some(function(b: GridPoint){
            return point.u == b.u && point.v == b.v;
        })) {
            // not a valid next move
            return;
        }
        if ( this.currentBorehead.use(this.map.strength(point.u, point.v)) ) {
            this.stepsTaken++;
            this.updateVisibility(point);
            this.map.set_drilled(point.u, point.v, true);
            this.map.boreDirection
                = new GridPoint(this.map.boreHead.u + point.u - this.map.boreHead.u,
                this.map.boreHead.v + point.v - this.map.boreHead.v);
            this.map.boreHead = point;
            this.path.push(new PathPoint(point));
            renderer.render(this.map);
            if(this.map.target(point.u, point.v)) {

                // we have a winner
                this.clearTimeout();
                this.map.set_visibilityToAll(true);
                renderer.render(this.map);
                this.displayMessage("Congratulations you've found the reservoir. You won!<br /><a href='index.html?level="+(this.currentLevel+2).toString()+"'>Next Level</a>");
                return;

            } else if(this.map.resource(point.u, point.v) !== null) {
                this.manager.increaseBudget(this.map.resource(point.u, point.v).value);

            }

            this.updateValidMoves();
            if(this.validMoves.length === 0) {
                this.clearTimeout();
                this.displayMessage("You are out of moves. You lose!<br /><a href='index.html?level="+(this.currentLevel+1).toString()+"'>Restart Level</a>");
                return;
            }
            this.updateGUI();
            this.setTimeout();

        } else {
            this.clearTimeout();
            this.displayMessage("You are out of money or your drillhead is broken, try buying a new one. <br /><a href='index.html?level="+(this.currentLevel+1).toString()+"'>Restart Level</a>");
            return;
        }
    }
    private clearTimeout() {
        window.clearTimeout(this.timeout);
    }
    private setTimeout() {
        this.timeout = window.setTimeout(this.autoMove.bind(this), 5000);
    }
    private validatePos(point : GridPoint) : boolean {
        return true;
    }
    updateVisibility(point : GridPoint) {
        this.currentVisible = [];
        var range = this.visibiltyRange;
        
        var currUp = Math.max(point.v - range, 0);
        var currDown = Math.min(point.v + range + 1, this.map.height);
        var width = range;
        for(var v = currUp; v < point.v; ++v) {
            for(var u = Math.max(point.u - width, 0); u < Math.min(point.u + width + 1, this.map.width); ++u) {
                this.currentVisible.push(new GridPoint(u, v));
            }
        }
        for(var v = point.v; v < currDown; ++v) {
            for(var u = Math.max(point.u - width, 0); u < Math.min(point.u + width + 1, this.map.width); ++u) {
                this.currentVisible.push(new GridPoint(u, v));
            }
        }
        this.currentVisible.forEach((p) => {
            this.map.set_visibility(p.u, p.v, true);
        });
    }
    makeColumnVisible(u : number) {
        for(var v = 0; v < this.map.height; ++v) {
            this.map.set_visibility(u, v, true);
        }
    }
    placeResources() {
        var resources = ['gold', 'diamond', 'fossil'];
        
        for(var i = 0; i<50; i++) {
            let what = Math.floor(Math.random() * 3);
            var u = Math.floor(Math.random() * (this.map.width - 1));
            var v = Math.floor(Math.random() * (this.map.height - 1));
            while(this.map.target(u,v) || (this.map.boreHead.u == u && this.map.boreHead.v == v) || this.map.resource(u, v) !== null) {
                u = Math.floor(Math.random() * (this.map.width - 1));
                v = Math.floor(Math.random() * (this.map.height - 1));
            }
            this.map.set_resource(u, v, makeResource(resources[what]));
        }
    }
    setupGUI() {
        this.gui.time = document.getElementById('timerLabel');
        this.gui.budget = document.getElementById('budgetLabel');
        this.gui.totalTime = document.getElementById('timeLabel');
        this.gui.steps = document.getElementById('stepLabel');
        this.gui.level = document.getElementById('levelLabel');
        this.gui.boreheadType = document.getElementById('boreheadType');
        this.gui.boreheadType.innerHTML = "iron";
        this.gui.durability = document.getElementById('drillDurabilityBar');
        this.gui.button = {};
        this.gui.button.seismics = document.getElementById('seismicsButton');
        this.gui.button.seismics.addEventListener('click', this.doSeismics.bind(this));

        this.gui.button.stabilize = document.getElementById('stabilizationButton');
        this.gui.button.stabilize.addEventListener('click', this.doStabilize.bind(this));

        this.gui.button.exploration = document.getElementById('explorationButton');
        this.gui.button.exploration.addEventListener('click', this.doExploration.bind(this));

        this.gui.button.buyIronDrill = document.getElementById('buyIronDrillButton');
        this.gui.button.buyIronDrill.addEventListener('click', this.buyIronDrill.bind(this));

        this.gui.button.buySteelDrill = document.getElementById('buySteelDrillButton');
        this.gui.button.buySteelDrill.addEventListener('click', this.buySteelDrill.bind(this));

        this.gui.button.buyDiamondDrill = document.getElementById('buyDiamondDrillButton');
        this.gui.button.buyDiamondDrill.addEventListener('click', this.buyDiamondDrill.bind(this));

        this.gui.dialog = document.getElementById('dialog');



        this.updateGUI();
    }
    updateGUI() {
        window.clearTimeout(this.timer);
        this.updateBudget();
        this.gui.level.innerHTML = "Level: " + (this.currentLevel+1).toString();
        this.gui.steps.innerHTML = "Steps: " + this.stepsTaken.toString();
        this.timeLeft = 5000;
        this.timer = window.setTimeout(this.updateProgress.bind(this), 20);

        let durability = this.currentBorehead.getDurability();
        let that = this;
        window.setTimeout(function() {
            that.gui.durability.MaterialProgress.setProgress(durability/MAX_DURA*100);
        }, 100);
    }

    updateProgress() {

        window.clearTimeout(this.timer);
        this.timeLeft -= 20;
        if ( this.timeLeft < 0 ) return;
        this.gui.time.innerHTML = "Remaining time: " + this.timeLeft.toString();
        this.timer = window.setTimeout(this.updateProgress.bind(this), 20);
    }

    buyDiamondDrill() {
        this.buyNewBorehead('diamond');
        this.updateBudget();
    }

    buySteelDrill() {
        this.buyNewBorehead('steel');
        this.updateBudget();
    }

    buyIronDrill() {
        this.buyNewBorehead('iron');
        this.updateBudget();
    }

    doSeismics() {
        if ( this.manager.buySeismics() ) {
            this.visibiltyRange += 1;
            this.updateVisibility(this.map.boreHead);
        }
        this.updateBudget();
    }

    doExploration() {
        if ( this.manager.buyExploration() ) {
            let u = Math.floor(Math.random()*this.map.width);
            this.makeColumnVisible(u);
            renderer.render(this.map);
        }
        this.updateBudget();

    }

    doStabilize() {
        
    }

    updateBudget() {
        this.gui.budget.innerHTML = "Budget: $" + this.manager.getBudget().toString();
    }

    displayMessage(msg: string) {
        let title = document.getElementById('dialogTitle');
        let content = document.getElementById('dialogContent');
        title.innerHTML = "Information";
        content.innerHTML = msg;
        this.gui.dialog.showModal();
    }
}
