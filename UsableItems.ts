import { Manager } from './Manager'

export var MAX_DURA = 26;

export class UsableItems {
    private durability: number;
    private durabilityDecrement: number;
    private manager : Manager;
    
    
    constructor(durabilityDecrement:number) {
        this.durability = MAX_DURA;
        this.durabilityDecrement = durabilityDecrement;
    }

    use(strength : number) {
        // we test this pre and postdecrementing, since we need to
        // know if we are allowed to advance a step and later on
        // we might want to destroy the borehead
        if ( this.durability <= 0 ) {
            return false;
        }
        var factor = 1; //strength / 1000.;
        this.durability -= factor * this.durabilityDecrement;
        if ( this.durability <= 0 ) {
            this.destroy();
        }
        return true;
    }
    destroy() {
        
    }
    getDurability() {
        return this.durability;
    }
    getDurabilityDecrement() {
        return this.durabilityDecrement;
    }
    setDurabilityDecrement(durabilityDecrement) {
        this.durabilityDecrement=durabilityDecrement;
    }
    setManager(manager:Manager) {
        this.manager = manager;
    }
    getManager() {
        return this.manager;
    }
}