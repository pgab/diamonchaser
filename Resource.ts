export class Resource {
    name : string;
    value : number;
    constructor(name : string, value : number) {
        this.name = name;
        this.value = value;
    }
}

export function makeResource(resourceS : string) : Resource {
    var resource;
    if(resourceS == 'gold') {
        resource = new Resource('gold', 1000);
    } else if(resourceS == 'diamond') {
        resource = new Resource('diamond', 5000);
    } else if(resourceS == 'fossil') {
        resource = new Resource('fossil', 500);
    } else {
        console.log("unknown resource");
    }
    return resource;
}
